package concretesolutions.com.br.desafio.service;

import java.util.List;

import concretesolutions.com.br.desafio.model.PullRequest;
import concretesolutions.com.br.desafio.model.Repository;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

/**
 * Created by Marcus on 19/10/16.
 */

public interface GitRepositoryServiceInterface {

    @GET("/search/repositories?q=language:Java&sort=stars&per_page=10")
    Call<Repository> getAll(@Query("page") int page);

    @GET("/repos/{login}/{repo}/pulls?per_page=10")
    Call<List<PullRequest>> geteAllPullReq(@Path("login") String login, @Path("repo") String repo, @Query("page") int page);
}