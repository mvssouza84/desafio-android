package concretesolutions.com.br.desafio.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

/**
 * Created by Marcus on 20/10/16.
 */

public class PullRequest {


    @SerializedName("html_url")
    private String htmlUrl;

    private OwnerUser user;

    private String body;

    @SerializedName("created_at")
    private Date createdAt;

    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getHtmlUrl() {
        return htmlUrl;
    }

    public void setHtmlUrl(String htmlUrl) {
        this.htmlUrl = htmlUrl;
    }

    public OwnerUser getUser() {
        return user;
    }

    public void setUser(OwnerUser user) {
        this.user = user;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
}
