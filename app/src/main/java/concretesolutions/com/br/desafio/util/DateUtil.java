package concretesolutions.com.br.desafio.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by Marcus on 20/10/16.
 */

public class DateUtil {

    public static String formatDate(Date date) {
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }
}
