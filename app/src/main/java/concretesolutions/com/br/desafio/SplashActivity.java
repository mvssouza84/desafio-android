package concretesolutions.com.br.desafio;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import concretesolutions.com.br.desafio.util.CheckInternet;

/**
 * Created by Marcus on 20/10/16.
 */

public class SplashActivity extends DefaultActivity {

    private static int SPLASH_TIME_OUT = 3000;
    public static SplashActivity splashActivity;

    @BindView(R.id.title) TextView title;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);

        splashActivity = this;

        if(CheckInternet.isOnline(this)) {
            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    Intent intent = new Intent(SplashActivity.this, MainActivity.class);

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(SplashActivity.this, title, getResources().getString(R.string.title_transition));

                    if(Build.VERSION.SDK_INT >= 21) {
                        startActivity(intent, options.toBundle());
                    }else {
                        startActivity(intent);
                    }

                   // finish();
                }
            }, SPLASH_TIME_OUT);
        }else {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.internat_failed), Toast.LENGTH_LONG);
            toast.show();
        }
    }
}
