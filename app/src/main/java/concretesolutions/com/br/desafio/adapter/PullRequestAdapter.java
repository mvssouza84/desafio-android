package concretesolutions.com.br.desafio.adapter;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import concretesolutions.com.br.desafio.R;
import concretesolutions.com.br.desafio.model.PullRequest;
import concretesolutions.com.br.desafio.util.DateUtil;

/**
 * Created by Marcus on 20/10/16.
 */

public class PullRequestAdapter extends RecyclerView.Adapter<PullRequestAdapter.ViewHolder> {

    private List<PullRequest> pullRequests;

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_pull_request, parent, false);
        return new PullRequestAdapter.ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        PullRequest pullRequest = pullRequests.get(position);
        holder.name.setText(pullRequest.getTitle());
        holder.ownerImage.setImageURI(Uri.parse(pullRequest.getUser().getAvatarUrl()));
        holder.login.setText(pullRequest.getUser().getLogin());
        holder.userName.setText(pullRequest.getUser().getLogin());
        holder.body.setText(pullRequest.getBody());
        holder.date.setText(DateUtil.formatDate(pullRequest.getCreatedAt()));
    }

    @Override
    public int getItemCount() {
        return pullRequests.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name) TextView name;
        @BindView(R.id.owner_img) SimpleDraweeView ownerImage;
        @BindView(R.id.login) TextView login;
        @BindView(R.id.user_name) TextView userName;
        @BindView(R.id.body) TextView body;
        @BindView(R.id.date) TextView date;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    PullRequest pullRequest = pullRequests.get(position);
                    String url = pullRequest.getHtmlUrl();

                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    itemView.getContext().startActivity(intent);
                }
            });
        }
    }

    public void setItems(List<PullRequest> pullRequests) {
        this.pullRequests = pullRequests;
    }
}