package concretesolutions.com.br.desafio;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

import concretesolutions.com.br.desafio.util.Constants;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Marcus on 19/10/16.
 */

public class App extends Application {


    @Override
    public void onCreate() {
        super.onCreate();
        Constants.RETROFIT = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        Fresco.initialize(this);
    }
}
