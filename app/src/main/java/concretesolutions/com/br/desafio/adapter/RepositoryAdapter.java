package concretesolutions.com.br.desafio.adapter;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.drawee.view.SimpleDraweeView;

import java.util.List;

import butterknife.BindDimen;
import butterknife.BindView;
import butterknife.ButterKnife;
import concretesolutions.com.br.desafio.MainActivity;
import concretesolutions.com.br.desafio.PullRequestActivity;
import concretesolutions.com.br.desafio.R;
import concretesolutions.com.br.desafio.SplashActivity;
import concretesolutions.com.br.desafio.model.Item;

/**
 * Created by Marcus on 20/10/16.
 */

public class RepositoryAdapter extends RecyclerView.Adapter<RepositoryAdapter.ViewHolder> {

    private List<Item> items;
    private int lastPosition = -1;
    private MainActivity mainActivity;

    public RepositoryAdapter(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_repo, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Item item = items.get(position);
        holder.repoName.setText(item.getName());
        holder.repoDescr.setText(item.getDescription());
        holder.ownerImg.setImageURI(Uri.parse(item.getOwner().getAvatarUrl()));
        holder.ownerUserName.setText(item.getOwner().getLogin());
        setAnimation(holder.container, position);
        holder.forkCount.setText(String.valueOf(item.getForks()));
        holder.starCount.setText(String.valueOf(item.getStargazersCount()));
    }

    @Override
    public int getItemCount() {
        return items.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.repo_name) TextView repoName;
        @BindView(R.id.repo_descr) TextView repoDescr;
        @BindView(R.id.owner_img) SimpleDraweeView ownerImg;
        @BindView(R.id.owner_username) TextView ownerUserName;
        @BindView(R.id.container) LinearLayout container;
        @BindView(R.id.fork_count) TextView forkCount;
        @BindView(R.id.star_count) TextView starCount;

        public ViewHolder(final View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    int position = getAdapterPosition();
                    Intent intent = new Intent(itemView.getContext(), PullRequestActivity.class);
                    intent.putExtra("repoName", items.get(position).getName());
                    intent.putExtra("login", items.get(position).getOwner().getLogin());

                    ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(mainActivity, repoName, itemView.getContext().getResources().getString(R.string.title_transition));

                    if(Build.VERSION.SDK_INT >= 21) {
                        itemView.getContext().startActivity(intent, options.toBundle());
                    }else {
                        itemView.getContext().startActivity(intent);
                    }


                }
            });
        }
    }

   public void setItems(List<Item> items) {
       this.items = items;
   }

    private void setAnimation(View viewToAnimate, int position)
    {
        if (position > lastPosition)
        {
            Animation animation = AnimationUtils.loadAnimation(viewToAnimate.getContext(), android.R.anim.slide_in_left);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;
        }
    }
}