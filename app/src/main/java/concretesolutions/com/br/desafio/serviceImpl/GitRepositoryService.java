package concretesolutions.com.br.desafio.serviceImpl;

import java.util.List;

import concretesolutions.com.br.desafio.MainActivity;
import concretesolutions.com.br.desafio.PullRequestActivity;
import concretesolutions.com.br.desafio.model.PullRequest;
import concretesolutions.com.br.desafio.model.Repository;
import concretesolutions.com.br.desafio.service.GitRepositoryServiceInterface;
import concretesolutions.com.br.desafio.util.Constants;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Marcus on 19/10/16.
 */

public class GitRepositoryService {

    private GitRepositoryServiceInterface gitRepoService;

    public GitRepositoryService() {
        gitRepoService = Constants.RETROFIT.create(GitRepositoryServiceInterface.class);

    }

    public void getAll(int page, final MainActivity mainActivity) {
        Call<Repository> call = gitRepoService.getAll(page);

        call.enqueue(new Callback<Repository>() {
            @Override
            public void onResponse(Call<Repository> call, Response<Repository> response) {
                if(response.errorBody() == null) {
                    if(response.body() != null) {
                        mainActivity.finishLoading(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<Repository> call, Throwable t) {

            }
        });
    }

    public void geteAllPullReq(String login, String repo, int page, final PullRequestActivity pullRequestActivity) {
        Call<List<PullRequest>> call = gitRepoService.geteAllPullReq(login, repo, page);

        call.enqueue(new Callback<List<PullRequest>>() {
            @Override
            public void onResponse(Call<List<PullRequest>> call, Response<List<PullRequest>> response) {
                if(response.errorBody() == null) {
                    if(response.body() != null) {
                        pullRequestActivity.finishLoading(response.body());
                    }
                }
            }

            @Override
            public void onFailure(Call<List<PullRequest>> call, Throwable t) {

            }
        });
    }

}