package concretesolutions.com.br.desafio;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindString;
import butterknife.BindView;
import butterknife.ButterKnife;
import concretesolutions.com.br.desafio.adapter.RepositoryAdapter;
import concretesolutions.com.br.desafio.listener.EndlessRecyclerOnScrollListener;
import concretesolutions.com.br.desafio.model.Item;
import concretesolutions.com.br.desafio.model.Repository;
import concretesolutions.com.br.desafio.serviceImpl.GitRepositoryService;
import concretesolutions.com.br.desafio.util.CheckInternet;

public class MainActivity extends DefaultActivity {

    GitRepositoryService gitRepoService;
    @BindView(R.id.rv_repositories) RecyclerView rvRepositories;
    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindString(R.string.app_name) String title;
    int initialPage = 1;
    LinearLayoutManager linearLayoutManager;
    RepositoryAdapter repositoryAdapter;
    List<Item> itemList;
    @BindView(R.id.progress_bar) FrameLayout progressBar;

    @Override
    protected void onCreate(@Nullable  Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if(CheckInternet.isOnline(this)) {
            inject();

            setupToolbar();

            setupRecyclerView();

            gitRepoService = new GitRepositoryService();

            gitRepoService.getAll(initialPage, this);
        }else {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.internat_failed), Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
    }

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        rvRepositories.setLayoutManager(linearLayoutManager);


        rvRepositories.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                gitRepoService.getAll(current_page, MainActivity.this);
            }
        });
    }


    private void inject() {
        ButterKnife.bind(this);
    }

    public void finishLoading(Repository repository) {
        List<Item> items = repository.getItems();
        if(items != null && items.size() > 0) {
            if(itemList == null) {
                itemList = new ArrayList<>();
                itemList.addAll(items);

                repositoryAdapter = new RepositoryAdapter(this);
                repositoryAdapter.setItems(itemList);
                rvRepositories.setAdapter(repositoryAdapter);
            }else {
                itemList.addAll(items);
                int curSize = repositoryAdapter.getItemCount();
                repositoryAdapter.notifyItemRangeInserted(curSize, itemList.size() - 1);
            }
        }
        progressBar.setVisibility(View.GONE);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == KeyEvent.KEYCODE_BACK) {
            SplashActivity.splashActivity.finish();
            finish();
            return true;
        }

        return super.onKeyDown(keyCode, event);
    }
}