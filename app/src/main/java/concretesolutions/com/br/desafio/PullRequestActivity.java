package concretesolutions.com.br.desafio;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import concretesolutions.com.br.desafio.adapter.PullRequestAdapter;
import concretesolutions.com.br.desafio.listener.EndlessRecyclerOnScrollListener;
import concretesolutions.com.br.desafio.model.PullRequest;
import concretesolutions.com.br.desafio.serviceImpl.GitRepositoryService;
import concretesolutions.com.br.desafio.util.CheckInternet;

/**
 * Created by Marcus on 20/10/16.
 */

public class PullRequestActivity extends DefaultActivity {

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.title) TextView title;
    @BindView(R.id.rv_pull_request) RecyclerView rvPullRequest;
    private Intent pIntent;
    private String login;
    private String repoName;
    GitRepositoryService gitRepoService;
    LinearLayoutManager linearLayoutManager;
    PullRequestAdapter pullRequestAdapter;
    List<PullRequest> pullRequestsList;
    @BindView(R.id.progress_bar) FrameLayout progressBar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_pull_request);

        if(CheckInternet.isOnline(this)) {
            inject();

            pIntent = getIntent();
            login = pIntent.getStringExtra("login");
            repoName = pIntent.getStringExtra("repoName");

            setupToolbar();

            setupRecyclerView();

            gitRepoService = new GitRepositoryService();
            gitRepoService.geteAllPullReq(login, repoName, 1, this);
        }else {
            Toast toast = Toast.makeText(this, getResources().getString(R.string.internat_failed), Toast.LENGTH_LONG);
            toast.show();
        }
    }

    private void setupRecyclerView() {
        linearLayoutManager = new LinearLayoutManager(this);
        rvPullRequest.setLayoutManager(linearLayoutManager);

        rvPullRequest.addOnScrollListener(new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                gitRepoService.geteAllPullReq(login, repoName, current_page, PullRequestActivity.this);
            }
        });
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        title.setText(repoName);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void inject() {
        ButterKnife.bind(this);
    }

    public void finishLoading(List<PullRequest> pullRequests) {
        if(pullRequests != null && pullRequests.size() > 0) {
            if(pullRequestsList == null) {
                pullRequestsList = new ArrayList<>();
                pullRequestsList.addAll(pullRequests);

                pullRequestAdapter = new PullRequestAdapter();
                pullRequestAdapter.setItems(pullRequestsList);
                rvPullRequest.setAdapter(pullRequestAdapter);
            }else {
                pullRequestsList.addAll(pullRequests);
                int curSize = pullRequestAdapter.getItemCount();
                pullRequestAdapter.notifyItemRangeInserted(curSize, pullRequestsList.size() - 1);
            }
        }
        progressBar.setVisibility(View.GONE);
    }
}
