package concretesolutions.com.br.desafio.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcus on 19/10/16.
 */

public class Item {

    private int id;

    private String name;

    @SerializedName("full_name")
    private String fullName;

    private OwnerUser owner;

    private String description;

    @SerializedName("stargazers_count")
    private int stargazersCount;

    private int forks;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public OwnerUser getOwner() {
        return owner;
    }

    public void setOwner(OwnerUser owner) {
        this.owner = owner;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getStargazersCount() {
        return stargazersCount;
    }

    public void setStargazersCount(int stargazersCount) {
        this.stargazersCount = stargazersCount;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        this.forks = forks;
    }
}
