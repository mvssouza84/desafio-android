package concretesolutions.com.br.desafio.model;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Marcus on 19/10/16.
 */

public class OwnerUser {

    private String login;

    @SerializedName("avatar_url")
    private String avatarUrl;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
